Gem::Specification.new do |s|
    s.name        = 'country_time_service'
    s.version     = '1.0.0'
    s.date        = '2018-11-01'
    s.summary     = "Gema para acceder al servicio de ejemplo de horario"
    s.description = "Gema para acceder al servicio de ejemplo de horario"
    s.authors     = ["Pablo Vilas"]
    s.email       = 'pablovilas89@gmail.com'
    s.files       = ["lib/country_time_service.rb"]
    s.license       = 'MIT'
    s.require_paths = ['lib']
    s.add_development_dependency 'bundler'
    s.add_runtime_dependency 'rest-client'
end