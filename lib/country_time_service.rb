require 'rest-client'
require 'json'

# https://guides.rubygems.org/make-your-own-gem/
# gem build country_time_service.gemspec

class CountryTimeService

    def time_zone(code)
        response = RestClient.get url, { params: { tz: code } }
        if response.code == 200
            JSON.parse(response.body)
        else
            raise "An error has occured. Response was #{response.code}"
        end
    end

    private

    def url
        'https://country-time-service.mybluemix.net'
    end

end